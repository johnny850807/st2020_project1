const _ = require('lodash');
const chai = require("chai");
const expect = chai.expect; 


test('1. Should reverse an array', () => {
    const array = [1, 2, 3];
    let reversedArr = _.reverse(array);
    expect(reversedArr).to.eql([3, 2, 1]);
});

test('2. Should fill an array with "A"', () => {
   const array = [1, 2, 3];
   let filledArray = _.fill(array, 'A');
   expect(filledArray).to.eql(['A', 'A', 'A']);
});

test('3. Given an array consists of 1~5, and each number repeats exactly two times. ' +
    'When count the array by the number, should get 2 from every key number', () => {
    const array = [1, 2, 3, 4, 5, 1, 2, 3, 4, 5];
    const countResult = _.countBy(array, Math.floor);
    for (let i = 1; i < 5; i++) {
        expect(countResult[i]).to.eql(2);
    }
});

test('4. Should capitalize a word', () => {
   const word = 'johnny850807';
   const capitalized = _.capitalize(word);
   expect(capitalized).to.eql('Johnny850807');
});


test('5. Should detect end with the given value', () => {
   const sentence = 'Hello! My world!';
   expect(_.endsWith(sentence, 'My world!')).to.eql(true);
});

test('6. Should add two numbers', () => {
   expect(_.add(1000, 2000)).to.eql(3000)
});

test('7. Should make a function which returns the constant given', () => {
    const myConstant = 1000;
    const func =_.constant(myConstant);
    expect(func()).to.eql(myConstant);
});

test('8. Should make a function that invokes a given function', () => {
    let x = 0;
    const func = _.ary(()=> x++, 0);
    func();
    expect(x).to.eql(1);
});

test('9. Should detect start with the given value', () => {
    const sentence = 'Hello! My world!';
    expect(_.startsWith(sentence, 'Hello!')).to.eql(true);
});


test('10. Should chunk an array', () => {
    const array = ['A', 'B', 'C', 'D'];
    const chunked = _.chunk(array, 3);
    expect(chunked).to.eql([['A', 'B', 'C'], ['D']])
});