let sorting = (array) => {
    return array.sort();
};

let compare = (a, b) => {
    return (a['PM2.5']).localeCompare(b['PM2.5']);
};

let average = (nums) => {
    return roundToPrecision(nums.reduce((a, b) => a + b, 0) / nums.length, 0.01);
};

function roundToPrecision(x, precision) {
    const y = x + (precision === undefined ? 0.5 : precision / 2);
    return y - (y % (precision === undefined ? 1 : +precision));
}

module.exports = {
    sorting,
    compare,
    average
};